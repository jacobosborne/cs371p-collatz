// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <sstream>  // istringtstream, ostringstream
#include <string>   // string
#include <utility>  // pair

#include "gtest/gtest.h"

#include "Collatz.h"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    string s("1 10\n");
    const pair<int, int> p = collatz_read(s);
    ASSERT_EQ(p.first,   1);
    ASSERT_EQ(p.second, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval_1) {
    const int v = collatz_eval(1, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval_2) {
    const int v = collatz_eval(100, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval_3) {
    const int v = collatz_eval(201, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval_4) {
    const int v = collatz_eval(900, 1000);
    ASSERT_EQ(v, 174);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream w;
    collatz_print(w, 1, 10, 20);
    ASSERT_EQ(w.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream r("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", w.str());
}

// ------
// custom
// ------

TEST(CollatzFixture, c_eval_1) {
    const int v = collatz_eval(1, 999999);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, c_eval_2) {
    const int v = collatz_eval(999990, 999999);
    ASSERT_EQ(v, 259);
}

TEST(CollatzFixtrue, c_eval_3) {
    const int v = collatz_eval(900000, 999999);
    ASSERT_EQ(v, 507);
}

// ----------------
// custom - reverse
// ----------------

TEST(CollatzFixture, c_reverse_1) {
    const int v = collatz_eval(1, 10);
    const int v_r = collatz_eval(10, 1);
    ASSERT_EQ(v_r, v);
}

TEST(CollatzFixture, c_reverse_2) {
    const int v = collatz_eval(999990, 999999);
    const int v_r = collatz_eval(999999, 999990);
    ASSERT_EQ(v_r, v);
}

TEST(CollatzFixture, c_reverse_3) {
    const int v = collatz_eval(1, 999999);
    const int v_r = collatz_eval(999999, 1);
    ASSERT_EQ(v_r, v);
}

// ------------
// custom - one
// ------------

TEST(CollatzFixture, c_one_1) {
    const int v = collatz_eval(1, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, c_one_2) {
    const int v = collatz_eval(10, 10);
    ASSERT_EQ(v, 7);
}

TEST(CollatzFixture, c_one_3) {
    const int v = collatz_eval(999999, 999999);
    ASSERT_EQ(v, 259);
}

// --------------
// custom - solve
// --------------

TEST(CollatzFixture, c_solve_1) {
    istringstream r("1 999999\n999999 1\n900000 999999\n999999 900000\n");
    ostringstream w;
    collatz_solve(r, w);
    ASSERT_EQ("1 999999 525\n999999 1 525\n900000 999999 507\n999999 900000 507\n", w.str());
}
