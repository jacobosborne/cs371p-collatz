// -----------
// Collatz.c++
// -----------

//#define DO_PRINT
#define IF_PRINT_CONDITION if (true)

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair

#include "Collatz.h"

using namespace std;

#include <map>
#include <list>
#ifdef DO_PRINT
#include <sstream>
#endif

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i;
    int j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

int collatz_eval (int i, int j) {
    assert(i > 0);
    assert(i < 1000000);
    assert(j > 0);
    assert(j < 1000000);

    int start_num = std::min(i, j);
    int end_num = std::max(i, j);


#define CACHE_SIZE 10000000
    static int lengths_cache[CACHE_SIZE];
#define CACHE(i) lengths_cache[i-1]

    /* Initialize cache array so that all values except 1 are -1, and 1 is 1 */
    if (CACHE(1) != 1) {
        CACHE(1) = 1;
        for (int idx = 2; idx <= CACHE_SIZE; idx++) {
            CACHE(idx) = -1;
        }
    }


    /* Iterate through all values in the range to find their cycle length, storing the longest */
    int max_cycles = 0;
    for (int idx = start_num; idx <= end_num; idx++) {

#ifdef DO_PRINT
        std::stringstream ss;
#endif

        long int n = idx;
        long int last_n = n;
        int cycles = 0;
        bool going = true;
        while (going) {
#ifdef DO_PRINT
            ss << "n: " << n << "\n";
#endif
            assert(n > 0);

            /* Attempt to find the value in the cache; if not found do the whole odd=3n+1, even=n/2 thing */
            if ( ( n <= CACHE_SIZE ) && ( CACHE(n) != -1 ) ) {
#ifdef DO_PRINT
                ss << "Found " << n << " in cache with length " << CACHE(n) << ".\n";
#endif
                cycles += CACHE(n);
                going = false;

            } else {
                last_n = n;
                if (n % 2 == 1) {
                    n = 3*n + 1;
                } else {
                    n = n/2;
                }
                cycles++;
            }
        }

#ifdef DO_PRINT
        ss << "Finished solving. Cycle Length is " << cycles << ".\n";
#endif

        // Update cache
#ifdef DO_PRINT
        ss << "Val to cache: " << last_n << "\n";
        if (last_n > CACHE_SIZE) ss << last_n << " is too big to cache (>" << CACHE_SIZE << ")!\n";
        if (idx <= 10) ss << "\n";
#endif
        if (last_n <= CACHE_SIZE && last_n != n) {
            CACHE(last_n) = CACHE(n) + 1;
        }

        if (cycles > max_cycles) {
            max_cycles = cycles;
#ifdef DO_PRINT
            ss << "Max length is now " << max_cycles << "\n";
#endif
        }
#ifdef DO_PRINT
        else ss << "Max length is still " << max_cycles << "\n";
#endif

#ifdef DO_PRINT
        IF_PRINT_CONDITION cout << ss.str() << endl;
#endif
    }

    assert(max_cycles > 0);
    return max_cycles;
}


// -------------
// collatz_print
// -------------

void collatz_print (ostream& w, int i, int j, int v) {
    w << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& r, ostream& w) {
    string s;
    while (getline(r, s)) {
        const pair<int, int> p = collatz_read(s);
        const int            i = p.first;
        const int            j = p.second;
        const int            v = collatz_eval(i, j);
        collatz_print(w, i, j, v);
    }
}
